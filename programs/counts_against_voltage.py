import sys
import math as ma
import statistics as stat
import matplotlib.pyplot as plt


voltages = [660., 710., 760., 810.,  860.,  910.,   960.,   1010., 1060.]
counts = [  6,  26,   336,  26498, 89425, 173280, 252320, 277798, 250085]
recommend_voltage = 960.
counts_at_recommend_voltage = 252320

if __name__ == '__main__':
    plt.plot(voltages, counts, marker='o', label='')
    plt.xlabel('voltage (V)')
    plt.ylabel('counts')
    plt.annotate('recommend voltage',
                 xy=(recommend_voltage, counts_at_recommend_voltage),
                 xytext=(recommend_voltage - 15,
                         counts_at_recommend_voltage-100000),
                 arrowprops=dict({'facecolor':'black', 'width':.8,
                                  'headwidth':4., 'headlength':7.,
                                  'shrink':.05}))
    plt.show()
