import sys
import math as ma
import os
import re
import statistics as stat
import matplotlib.pyplot as plt

import lib


files = ['first_he_spectrum.Spe', 'second_he_spectrum.Spe',
         'first_bf_spectrum.Spe', 'second_bf_spectrum.Spe']
fig, axs = plt.subplots(2, 2, sharex=True, sharey=True)
domain = list(range(512))
for i, file in enumerate(files):
    counts = lib.get_counts(file)
    axs[i//2, i%2].plot(domain, counts)
    axs[i//2, i%2].semilogy()

    if i//2 == 1:
        axs[i//2, i%2].set_xlabel('count')
    if i%2 == 0:
        axs[i//2, i%2].set_ylabel('channel')

    title = file[:-len('.Spe')]
    title = title.replace('_', ' ')
    title = title.replace(' bf ', ' BF3 ')
    title = title.replace(' he ', ' 3He ')
    title = title.replace('first', 'First')
    title = title.replace('second', 'Second')
    axs[i//2, i%2].set_title(title)

    if i == 0:
        axs[i//2, i%2].annotate('1H', xy=(80, 3000),
                                xytext=(70, 17000),
                                arrowprops=lib.arrowprops)
        axs[i//2, i%2].annotate('3H', xy=(185, 2000),
                                xytext=(175, 17000),
                                arrowprops=lib.arrowprops)
        axs[i//2, i%2].annotate('1H and 3H', xy=(325, 900),
                                xytext=(330, 7000),
                                arrowprops=lib.arrowprops)
    if i == 2:
        axs[i//2, i%2].annotate('1H', xy=(45, 850),
                                xytext=(40, 3000),
                                arrowprops=lib.arrowprops)
        axs[i//2, i%2].annotate('4He', xy=(85, 900),
                                xytext=(75, 200),
                                arrowprops=lib.arrowprops)
        axs[i//2, i%2].annotate('1H and 4He', xy=(130, 4000),
                                xytext=(180, 4000),
                                arrowprops=lib.arrowprops)
        axs[i//2, i%2].annotate('γ', xy=(160, 270),
                                xytext=(195, 270),
                                arrowprops=lib.arrowprops)




plt.show()
